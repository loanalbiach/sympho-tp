<?php

namespace App\Entity;

use App\Repository\AuteurRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AuteurRepository::class)]
class Auteur
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\OneToMany(mappedBy: 'Auteur', targetEntity: Article::class)]
    private Collection $liste_articles;

    public function __construct()
    {
        $this->liste_articles = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): static
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * @return Collection<int, Article>
     */
    public function getListeArticles(): Collection
    {
        return $this->liste_articles;
    }

    public function addListeArticle(Article $listeArticle): static
    {
        if (!$this->liste_articles->contains($listeArticle)) {
            $this->liste_articles->add($listeArticle);
            $listeArticle->setAuteur($this);
        }

        return $this;
    }

    public function removeListeArticle(Article $listeArticle): static
    {
        if ($this->liste_articles->removeElement($listeArticle)) {
            // set the owning side to null (unless already changed)
            if ($listeArticle->getAuteur() === $this) {
                $listeArticle->setAuteur(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->nom;
    }
}
