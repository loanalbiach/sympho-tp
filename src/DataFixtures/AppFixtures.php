<?php

namespace App\DataFixtures;

use App\Entity\Tags;
use App\Entity\User;
use App\Entity\Auteur;
use App\Entity\Article;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $auteur1 = new Auteur();
        $auteur1->setNom('Laury');
        $manager->persist($auteur1);

        $auteur2 = new Auteur();
        $auteur2->setNom('Belliot');
        $manager->persist($auteur2);

        $auteur3 = new Auteur();
        $auteur3->setNom('Albiach');
        $manager->persist($auteur3);

        $Article1 = new Article();
        $Article1->setTitre("L'alcool pour les gros nullos");
        $Article1->setAuteur($auteur3);
        $Article1->setDescription("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        $manager->persist($Article1);

        $Article2 = new Article();
        $Article2->setTitre("Les douches à gaz");
        $Article2->setAuteur($auteur2);
        $Article2->setDescription("Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of 'de Finibus Bonorum et Malorum' (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, 'Lorem ipsum dolor sit amet..', comes from a line in section 1.10.32.");
        $manager->persist($Article2);

        $Article3 = new Article();
        $Article3->setTitre('Les lapinos à la plage');
        $Article3->setAuteur($auteur1);
        $Article3->setDescription("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).");
        $manager->persist($Article3);
        
        $Article4 = new Article();
        $Article4->setTitre('Les marseillais en antarctique');
        $Article4->setAuteur($auteur1);
        $Article4->setDescription("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).");
        $manager->persist($Article4);

        $Tag1 = new Tags();
        $Tag1->setNom("Science");
        $Tag1->addArticle($Article3);
        $Tag1->addArticle($Article2);
        $manager->persist($Tag1);

        $Tag2 = new Tags();
        $Tag2->setNom("Fiction");
        $Tag2->addArticle($Article3);
        $Tag2->addArticle($Article1);
        $manager->persist($Tag2);

        $manager->flush();
    }
}
