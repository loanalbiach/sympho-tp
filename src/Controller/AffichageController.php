<?php

namespace App\Controller;

use App\Entity\Auteur;
use App\Form\AuteurType;
use App\Repository\TagsRepository;
use App\Repository\AuteurRepository;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class AffichageController extends AbstractController
{
    #[Route('/user/accueil', name: 'app_index')]
    public function index( ArticleRepository $articles ): Response{
        $lesArticles = $articles->findAll();
        return $this->render(
            'affichage/index.html.twig',
            [
                'articles' => $lesArticles
            ]
        );
    }

    #[Route('/user/auteurs', name: 'app_auteurs')]
    public function auteurs( AuteurRepository $auteurs ): Response{
        $lesAuteurs = $auteurs->findAll();
        return $this->render(
            'affichage/auteurs.html.twig',
            [
                'auteurs' => $lesAuteurs
            ]
        );
    }

    #[Route('/admin', name: 'app_admin')]
    public function admin(): Response
    {
        return $this->render('affichage/admin.html.twig');
    }

    #[Route('/user/auteurs/{nom}', name: 'app_unAuteur')]
    public function unAuteur(string $nom, AuteurRepository $auteurs, ArticleRepository $articles): Response{
        $unAuteur = $auteurs->findOneBy(['nom' => $nom]);;
        $lesArticles = $articles->findAll();
        return $this->render(
            'affichage/unAuteur.html.twig',
            [
                'nom' => $nom,
                'auteurs' => $unAuteur,
                'articles' => $lesArticles
            ]
        );
    }

    #[Route('/user/add/auteur', name: 'app_addAuteur')]
    public function addAuteur(Request $request, EntityManagerInterface $manager): Response{
        $auteur = new Auteur();

        $form = $this->createForm(AuteurType::class, $auteur);
        
        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()){
            $manager->persist($form->getData());
            $manager->flush();
            $this->addFlash('reussit', 'L\'auteur a été ajouté avec succès.');
            return $this->redirectToRoute('app_auteurs');
        }
        return $this->render('affichage/addAuteur.html.twig',
            [
                'form' => $form
            ]
        );
    }

    #[Route('/user/edit/auteur/{id}', name: 'app_editAuteur')]
    public function editAuteur(Auteur $id, Request $request, EntityManagerInterface $manager): Response{
        
        $form = $this->createForm(AuteurType::class, $id);

        $form->handleRequest($request);
        if($form->isSubmitted()&&$form->isValid()){
            $manager->persist($form->getData());
            $manager->flush();
            $this->addFlash('reussit', 'L\'auteur a été modifié avec succès.');
            return $this->redirectToRoute('app_auteurs');
        }
        return $this->render('affichage/editAuteur.html.twig',
            [
                'form' => $form
            ]
        );
    }

    #[Route('/user/remove/auteur/{id}', name: 'app_removeAuteur')]
    public function deleteAuteur(Auteur $id, EntityManagerInterface $manager): Response
    {
        $manager->remove($id);
        $manager->flush();

        $this->addFlash('reussit', 'L\'auteur a été supprimé avec succès.');
        return $this->redirectToRoute('app_auteurs');
    }
}
